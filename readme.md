# Whisk Editor

This is a simple 3D modeling program. You place and connect individual vertices to build meshes from triangles. This is a simple and easy alternative to more powerful modeling tools like Blender, and is great for low-poly work.

### IMPORTANT! - This project uses submodules. Use the following commands to run the code.
1. `git clone https://gitlab.com/sdggames/whisk` - Clone the project (if you haven't already)
2. `cd project` - Enter the project folder.
3. `git submodule update --init`