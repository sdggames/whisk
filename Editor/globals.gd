class_name AutoloadGlobal extends Node2D

# Joystick to mouse sensitivity.
@export var joystick_acceleration: float = 1000.0
@export var max_joystick_speed = 1200.0
@export var joystick_rotation_speed = 2400.0

# Is the mouse currently inside the editor?
var is_mouse_in_editor := false

enum Planes { X, Y, Z, NONE }
enum Mode { PLACE, MODIFY }
enum PlaceMode { SINGLE, TRIANGLE, RADIAL, ZIPPER }
enum ColorMode { SINGLE, FILL_SAME, FILL_ALL }

var mode := Mode.PLACE
var place_mode := PlaceMode.ZIPPER
var color_mode := ColorMode.SINGLE
var back_culling := false
var active_color := 0
var colors: Array[Color] = [Color.RED, Color.YELLOW, Color.GREEN, Color.TURQUOISE, Color.PURPLE, Color.GRAY, Color.hex(00000086)]
var mirror: Array[bool] = [false, false, false]
var menu_open := false

var vertex_id = 0
## A public list of all verts for both the VertexEditor and MeshBuilder to use. Why? Because I didn't plan ahead!
var all_points: Dictionary

## IDs to use to identify vertexes. IDs are given out sequentially.
func get_next_id():
	vertex_id += 1
	return vertex_id
