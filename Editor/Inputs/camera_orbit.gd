extends Node3D
@onready var camera_3d = $Camera3D
@export var camera_limits := Vector2(20, 150)

# Sensitivity of the mouse movement
@export var sensitivity = 0.05
@export var zoom_speed = 1.0

# Current rotation angles
var yaw = 0.0
var pitch = 0.0

# Initial mouse position
var initial_mouse_position: Vector2
var is_rotating = false

# Platform-specific sensitivity adjustment
var platform_sensitivity_multiplier := 1.0

func _ready():
	yaw = rotation_degrees.y
	pitch = rotation_degrees.x
	
	# Adjust sensitivity based on the platform
	if OS.get_name() == "HTML5":
		platform_sensitivity_multiplier = 0.5  # Adjust this value as needed

func _process(_delta):
	if Input.is_action_just_pressed("shift"):
		# Store the initial mouse position
		initial_mouse_position = get_viewport().get_mouse_position()
		is_rotating = true
	
	elif Input.is_action_just_released("shift"):
		# Stop rotating
		is_rotating = false
	
	elif Input.is_action_pressed("shift") and is_rotating:
		# Capture the mouse movement relative to the initial position
		var mouse_movement = get_viewport().get_mouse_position() - initial_mouse_position
		
		# Calculate the new rotation angles
		yaw -= mouse_movement.x * sensitivity * platform_sensitivity_multiplier
		pitch -= mouse_movement.y * sensitivity * platform_sensitivity_multiplier
		pitch = clamp(pitch, -80, 80)  # Limit the pitch to avoid gimbal lock

		# Apply the rotation to the Node
		rotation_degrees = Vector3(pitch, yaw, 0)

		# Reset the mouse position to the center of the viewport (doesn't work on web)
		get_viewport().warp_mouse(get_viewport().get_visible_rect().size * 0.5)

		# Update the initial mouse position for the next frame
		initial_mouse_position = get_viewport().get_mouse_position()
	
	# Camera movement when Shift is held and mouse wheel is scrolled
	if Input.is_action_pressed("shift"):
		# Just pressed registers the actual mouse, is pressed registers the joystick.
		if Input.is_action_pressed("mouse_wheel_down") or Input.is_action_just_pressed("mouse_wheel_down"):
			camera_3d.position.z = clamp(camera_3d.position.z + zoom_speed, camera_limits.x, camera_limits.y)
			camera_3d.position.x = camera_3d.position.z / 20.0
		elif Input.is_action_pressed("mouse_wheel_up") or Input.is_action_just_pressed("mouse_wheel_up"):
			camera_3d.position.z = clamp(camera_3d.position.z - zoom_speed, camera_limits.x, camera_limits.y)
			camera_3d.position.x = camera_3d.position.z / 20.0
