extends HBoxContainer

signal color_set(num: int, color: Color, colors_changed: bool)
signal color_changing(num: int, color: Color)
signal picker_open
signal picker_closed

@export var id := 1

var color_changed := false

@onready var button = $Button
@onready var color_picker_button = $ColorPickerButton


func _ready():
	button.text = "Color " + str(id + 1)
	color_picker_button.color = Global.colors[id]


func load_state():
	color_picker_button.color = Global.colors[id]
	button.button_pressed = Global.active_color == id


func toggle_on():
	button.button_pressed = true


func _on_button_pressed():
	color_set.emit(id, color_picker_button.color, false)


func _on_color_picker_button_color_changed(color):
	color_changed = true
	color_changing.emit(id, color)


func _on_color_picker_button_popup_closed():
	color_set.emit(id, color_picker_button.color, color_changed)
	color_changed = false
	toggle_on()
	picker_closed.emit()


func _on_color_picker_button_pressed():
	picker_open.emit()
