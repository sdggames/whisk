class_name CaptureMouse extends Sprite2D
## Class captures a joystick or keyboard input and treats it as mouse movements.

@onready var editor_space: ReferenceRect = $"../Gui/HBoxContainer/EditorSpace"

# Capture the screen size for boundary checks
var screen_size: Vector2 = Vector2()
var move_speed := 0.0
var saved_mouse_pos := Vector2.ZERO


func _ready():
	# Get the screen size
	screen_size = get_viewport().get_visible_rect().size
	get_viewport().size_changed.connect(_on_viewport_size_changed)


func _process(delta: float):
	var mouse_position = get_normalized_mouse_position()
	
	# Reset mouse position (gets changed during rotation)
	if Input.is_action_just_pressed("shift"):
		saved_mouse_pos = mouse_position
	if Input.is_action_just_released("shift"):
		set_normalized_mouse_position(saved_mouse_pos)
	
	# Update global check for mouse in editor.
	var editor_rect := editor_space.get_rect()
	Global.is_mouse_in_editor = editor_rect.has_point(get_viewport().get_mouse_position()) and not Global.menu_open
	
	# Get joystick axis values
	var joy_left_right = Input.get_axis("mouse_left", "mouse_right")
	var joy_up_down = Input.get_axis("mouse_up", "mouse_down")
	var joy_move = Vector2(joy_left_right, joy_up_down)
	
	if joy_left_right != 0 or joy_up_down != 0:
		# Get the current mouse position.
		var new_mouse_position = mouse_position
		
		if Input.is_action_pressed("shift"):
			new_mouse_position += joy_move * delta * Global.joystick_rotation_speed
		else:
			# Accelerate over time.
			var max_speed = Global.max_joystick_speed * min(joy_move.length_squared(), 1)
			move_speed = min(max_speed, move_speed + Global.joystick_acceleration * delta)
			
			# Calculate the mouse movement
			var mouse_move = joy_move * move_speed * delta
			
			# Update the mouse position
			new_mouse_position += mouse_move
			
		# Clamp the new mouse position within the normalized screen bounds
		new_mouse_position.x = clamp(new_mouse_position.x, 0, 1)
		new_mouse_position.y = clamp(new_mouse_position.y, 0, 1)
		
		# Set the mouse position
		set_normalized_mouse_position(new_mouse_position)
		update_pos(new_mouse_position)
	else:
		move_speed = 5.0
		update_pos(get_normalized_mouse_position())
		
	# Check for a specific generic input event
	if Input.is_action_just_pressed("mouse_lclick"):
		var a = InputEventMouseButton.new()
		a.position = get_global_mouse_position() * get_viewport_transform().get_scale()
		a.button_index = MOUSE_BUTTON_LEFT
		a.pressed = true
		Input.parse_input_event(a)
		await get_tree().process_frame
		var b = a.duplicate()
		b.pressed = false
		Input.parse_input_event(b)
	
	if Input.is_action_just_pressed("mouse_rclick"):
		var a = InputEventMouseButton.new()
		a.position = get_global_mouse_position() * get_viewport_transform().get_scale()
		a.button_index = MOUSE_BUTTON_RIGHT
		a.pressed = true
		Input.parse_input_event(a)
		await get_tree().process_frame
		var b = a.duplicate()
		b.pressed = false
		Input.parse_input_event(b)


## Set the position on the screen, and either show this sprite, or the actual mouse cursor.
func update_pos(pos: Vector2):
	# Hide the mouse, we're rotating instead of mousing.
	if Input.is_action_pressed("shift"):
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		visible = false
	elif Global.is_mouse_in_editor:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		visible = true
		position = Vector2(pos.x * screen_size.x, pos.y * screen_size.y)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		visible = false


# Helper functions to work with normalized coordinates
func get_normalized_mouse_position() -> Vector2:
	var mouse_position = get_viewport().get_mouse_position()
	return Vector2(mouse_position.x / screen_size.x, mouse_position.y / screen_size.y)


func set_normalized_mouse_position(pos: Vector2):
	get_viewport().warp_mouse(Vector2(pos.x * screen_size.x, pos.y * screen_size.y))


# Update screen size on viewport size change
func _on_viewport_size_changed():
	screen_size = get_viewport().get_visible_rect().size
