class_name Menu extends Panel
## Captures and processes all inputs. Emits signals that the rest of the application can use.
##
## Basically, the menu handles the controller inputs and keyboard shortuts, along with actual
## button presses. The only thing this doesn't completely handle is mouse inputs, because
## I just wrote the CaptureMouse and CameraOrbit code and don't care to refactor.

signal select_plane(plane: AutoloadGlobal.Planes)
signal scroll_plane(direction: int)
signal click_left
signal click_right
signal mode_changed
signal undo
signal redo
signal new
signal save(file)
signal load(file)
signal export(file)
signal reset_plane
signal revert_plane
signal colors_changing
signal colors_changed
signal next_back
signal prev_back
signal culling_changed

var can_scroll := true
var picking_color := false
var scroll_time_max: float
var scroll_time_min: float

var _log: Logger

@onready var scroll_timer = $ScrollTimer
@onready var save_dialog = $SaveDialog
@onready var load_dialog = $LoadDialog
@onready var export_dialog = $ExportDialog
@onready var controller_support = $ControllerSupport
@onready var back_culling = $MenuMargin/VBox/MirrorMode/Back_Culling
@onready var vertex_label = $"MenuMargin/VBox/Vertex Label"
@onready var mode = $MenuMargin/VBox/Mode
@onready var colors = [$MenuMargin/VBox/Colors/Color1, $MenuMargin/VBox/Colors/Color2, $MenuMargin/VBox/Colors/Color3, $MenuMargin/VBox/Colors/Color4, $MenuMargin/VBox/Colors/Color5, $MenuMargin/VBox/Colors/Color6, $MenuMargin/VBox/Colors/Color7]
@onready var mirror_x = $MenuMargin/VBox/MirrorMode/Mirror_X
@onready var mirror_y = $MenuMargin/VBox/MirrorMode/Mirror_Y
@onready var mirror_z = $MenuMargin/VBox/MirrorMode/Mirror_Z
@onready var select_x = $MenuMargin/VBox/PlaneSelect/Select_X
@onready var select_y = $MenuMargin/VBox/PlaneSelect/Select_Y
@onready var select_z = $MenuMargin/VBox/PlaneSelect/Select_Z
@onready var select_none = $MenuMargin/VBox/PlaneSelect/Select_None


func _ready():
	_log = Print.get_logger("Menu")
	scroll_time_max = scroll_timer.wait_time * 5
	scroll_time_min = scroll_timer.wait_time
	save_dialog.current_file = "whisk1.tres"
	export_dialog.current_file = "whisk1.glb"


# Handles most keyboard and joystick events.
func _process(_delta):
	# Input handling. Use code instead of shortcuts so triggers work, and we can respect Shift properly.
	if Global.menu_open:
		return # We are in a menu. Stop listening to the keyboard.
	
	# Hints:
	controller_support.visible = Input.is_action_pressed("peek_controls_joy")
	if Input.is_action_just_pressed("save"):
		_on_save_pressed()
	if Input.is_action_just_pressed("new"):
		_on_new_pressed()
	
	# Mouse wheel. Ignore shift, that is handled by camera.
	if !Input.is_action_pressed("shift"):
		if Input.is_action_just_pressed("mouse_wheel_down"): # Mouse wheel.
			_log.verbose("Scrolling plane from mouse.")
			scroll_plane.emit(1)
		elif Input.is_action_pressed("mouse_wheel_down"): # Joystick.
			if can_scroll:
				_log.verbose("Scrolling plane from joystick.")
				scroll_plane.emit(1)
				can_scroll = false
				scroll_timer.start(lerp(scroll_time_max, scroll_time_min, Input.get_action_strength("mouse_wheel_down")))
		elif Input.is_action_just_pressed("mouse_wheel_up"): # Mouse wheel.
			_log.verbose("Scrolling plane from mouse.")
			scroll_plane.emit(-1)
		elif Input.is_action_pressed("mouse_wheel_up"): # Joystick.
			if can_scroll:
				_log.verbose("Scrolling plane from joystick.")
				scroll_plane.emit(-1)
				can_scroll = false
				scroll_timer.start(lerp(scroll_time_max, scroll_time_min, Input.get_action_strength("mouse_wheel_up")))
		
		# Snap planes. (Same stick, or mouse wheel).
		if Input.is_action_just_pressed("plane_reset"):
			_on_reset_plane_pressed()
		if Input.is_action_just_pressed("plane_to_point"):
			_on_plane_to_vert_pressed()
	
	# Select plane or mirror plane.
	if Input.is_action_pressed("shift"): # Mirror
		if Input.is_action_just_pressed("select_X") and \
			!Input.is_action_just_pressed("redo"):
			mirror_x.button_pressed = !mirror_x.button_pressed
		if Input.is_action_just_pressed("select_Y"):
			mirror_y.button_pressed = !mirror_y.button_pressed
		if Input.is_action_just_pressed("select_Z"):
			mirror_z.button_pressed = !mirror_z.button_pressed
		if Input.is_action_just_pressed("select_none"):
			_on_mirror_none_pressed()
	else: # Select.
		if Input.is_action_just_pressed("select_X") and \
			!Input.is_action_just_pressed("undo"):
			select_x.button_pressed = true
			_on_select_x_pressed()
		if Input.is_action_just_pressed("select_Y"):
			select_y.button_pressed = true
			_on_select_y_pressed()
		if Input.is_action_just_pressed("select_Z"):
			select_z.button_pressed = true
			_on_select_z_pressed()
		if Input.is_action_just_pressed("select_none"):
			select_none.button_pressed = true
			_on_select_none_pressed()
	
	# Culling toggle
	if Input.is_action_just_pressed("back_culling"):
		back_culling.button_pressed = !back_culling.button_pressed
	
	# Undo, redo share a shortcut, make sure to process them in the right order.
	# Also, shares a key with backgrounds on controller.
	if Input.is_action_pressed("shift") and Input.is_action_just_pressed("previous_background"):
		_on_previous_background_pressed()
	elif Input.is_action_pressed("shift") and Input.is_action_just_pressed("next_background"):
		_on_next_background_pressed()
	elif Input.is_action_just_pressed("redo"):
		_on_redo_pressed()
	elif Input.is_action_just_pressed("undo"):
		_on_undo_pressed()
	if Input.is_action_just_pressed("change_background_this_one_is_keyboard_only_because_I_got_lazy_and_this_whole_thing_is_a_mess"):
		# What he said ^^^
		_on_next_background_pressed()
	
	# Keyboard.
	if Input.is_action_just_pressed("previous_color") and !Input.is_action_just_pressed("next_primary"):
			Global.active_color = wrapi(Global.active_color + 1, 0, Global.colors.size())
			colors[Global.active_color].toggle_on()
	if Input.is_action_just_pressed("next_color") and !Input.is_action_just_pressed("next_secondary"):
		Global.active_color = wrapi(Global.active_color - 1, 0, Global.colors.size())
		colors[Global.active_color].toggle_on()
	# Controller.
	if Input.is_action_pressed("shift"):
		if Input.is_action_just_pressed("previous_color"):
			Global.active_color = wrapi(Global.active_color + 1, 0, Global.colors.size())
			colors[Global.active_color].toggle_on()
		if Input.is_action_just_pressed("next_color"):
			Global.active_color = wrapi(Global.active_color - 1, 0, Global.colors.size())
			colors[Global.active_color].toggle_on()
	# Change primary and secondary modes.
	else:
		if Input.is_action_just_pressed("next_primary") and !Input.is_action_just_pressed("next_secondary"):
			match Global.mode:
				Global.Mode.PLACE:
					_on_move_pressed()
				Global.Mode.MODIFY:
					_on_place_pressed()
		if Input.is_action_just_pressed("next_secondary"):
			match Global.mode:
				Global.Mode.PLACE:
					match Global.place_mode:
						Global.PlaceMode.SINGLE:
							_on_place_triangle_pressed()
						Global.PlaceMode.TRIANGLE:
							_on_place_radial_pressed()
						Global.PlaceMode.RADIAL:
							_on_place_stitch_pressed()
						Global.PlaceMode.ZIPPER:
							_on_place_point_pressed()
				Global.Mode.MODIFY:
					pass # No secondary mode here.
	
	# Keyboard mode shortcuts.
	if Input.is_action_just_pressed("place_mode"):
		_on_place_pressed()
	if Input.is_action_just_pressed("move_mode"):
		_on_move_pressed()
	if Input.is_action_just_pressed("mode_sub1"):
		_on_place_point_pressed()
	if Input.is_action_just_pressed("mode_sub2"):
		_on_place_triangle_pressed()
	if Input.is_action_just_pressed("mode_sub3"):
		_on_place_radial_pressed()
	if Input.is_action_just_pressed("mode_sub4"):
		_on_place_stitch_pressed()


# Mouse inputs need to be handled in _input. The rest should be fine in _process.
func _input(event):
	if Global.is_mouse_in_editor and !Input.is_action_pressed("shift") and !picking_color:
		if event is InputEventMouseButton and event.pressed:
			if event.button_index == MOUSE_BUTTON_LEFT:
				_log.verbose("Click left")
				click_left.emit()
			elif event.button_index == MOUSE_BUTTON_RIGHT:
				_log.verbose("Click right")
				click_right.emit()


func update_active_point(pos: Vector3):
	vertex_label.text = "Vertex: " + str(pos)


func load_state():
	update_mode()
	mirror_x.button_pressed = Global.mirror[0]
	mirror_y.button_pressed = Global.mirror[1]
	mirror_z.button_pressed = Global.mirror[2]
	back_culling.button_pressed = Global.back_culling


## Updates the placement mode text, then emits mode_changed signal.
func update_mode():
	var label := "Mode: "
	match Global.mode:
		Global.Mode.PLACE:
			label += "Place "
			match Global.place_mode:
				Global.PlaceMode.SINGLE:
					label += "Single."
				Global.PlaceMode.TRIANGLE:
					label += "Triangle."
				Global.PlaceMode.RADIAL:
					label += "Radial."
				Global.PlaceMode.ZIPPER:
					label += "Sequential."
		Global.Mode.MODIFY:
			label += "Modify."
	mode.text = label
	mode_changed.emit()


# Menu funcitons. Save, load, export.
func _on_new_pressed():
	_log.debug("_on_new_pressed")
	new.emit()


func _on_save_pressed():
	_log.debug("_on_save_pressed")
	save_dialog.visible = true
	Global.menu_open = true


func _on_load_pressed():
	_log.debug("_on_load_pressed")
	load_dialog.visible = true
	Global.menu_open = true


func _on_export_pressed():
	_log.debug("_on_export_pressed")
	export_dialog.visible = true
	Global.menu_open = true


func _on_save_file_selected(path: String):
	Global.menu_open = false
	var dir_path = path.get_base_dir()
	var file_name = path.get_file()
	if load_dialog.current_file == "":
		load_dialog.current_file = file_name
		load_dialog.current_dir = dir_path
	if export_dialog.current_file == "whisk1.glb":
		export_dialog.current_file = file_name.replace(".tres", ".glb")
		export_dialog.current_dir = dir_path
	save.emit(path)

func _on_load_file_selected(path: String):
	Global.menu_open = false
	var dir_path = path.get_base_dir()
	var file_name = path.get_file()
	if save_dialog.current_file == "whisk1.tres":
		save_dialog.current_file = file_name
		save_dialog.current_dir = dir_path
	if export_dialog.current_file == "whisk1.glb":
		export_dialog.current_file = file_name.replace(".tres", ".glb")
		export_dialog.current_dir = dir_path
	load.emit(path)

func _on_export_file_selected(path: String):
	Global.menu_open = false
	export.emit(path)


func _on_undo_pressed():
	_log.debug("_on_undo_pressed")
	undo.emit()


func _on_redo_pressed():
	_log.debug("_on_redo_pressed")
	redo.emit()


# Primary modes. Place, move, color.
func _on_place_pressed():
	_log.debug("_on_place_pressed")
	Global.mode = Global.Mode.PLACE
	update_mode()


func _on_move_pressed():
	_log.debug("_on_move_pressed")
	Global.mode = Global.Mode.MODIFY
	update_mode()


# Placement secondary modes.
func _on_place_point_pressed():
	_log.debug("_on_place_point_pressed")
	Global.mode = Global.Mode.PLACE
	Global.place_mode = Global.PlaceMode.SINGLE
	update_mode()


func _on_place_triangle_pressed():
	_log.debug("_on_place_triangle_pressed")
	Global.mode = Global.Mode.PLACE
	Global.place_mode = Global.PlaceMode.TRIANGLE
	update_mode()


func _on_place_radial_pressed():
	_log.debug("_on_place_radial_pressed")
	Global.mode = Global.Mode.PLACE
	Global.place_mode = Global.PlaceMode.RADIAL
	update_mode()


func _on_place_stitch_pressed():
	_log.debug("_on_place_stitch_pressed")
	Global.place_mode = Global.PlaceMode.ZIPPER
	update_mode()


# Active plane management.
func _on_select_x_pressed():
	_log.debug("_on_select_x_pressed")
	select_plane.emit(Global.Planes.X)


func _on_select_y_pressed():
	_log.debug("_on_select_y_pressed")
	select_plane.emit(Global.Planes.Y)


func _on_select_z_pressed():
	_log.debug("_on_select_z_pressed")
	select_plane.emit(Global.Planes.Z)


func _on_select_none_pressed():
	_log.debug("_on_select_none_pressed")
	select_plane.emit(Global.Planes.NONE)


func _on_reset_plane_pressed():
	_log.debug("_on_reset_plane_pressed")
	reset_plane.emit()


func _on_plane_to_vert_pressed():
	_log.debug("_on_plane_to_vert_pressed")
	revert_plane.emit()


# Mirror Modes.
func _on_mirror_x_toggled(toggled_on):
	Global.mirror[0] = toggled_on


func _on_mirror_y_toggled(toggled_on):
	Global.mirror[1] = toggled_on


func _on_mirror_z_toggled(toggled_on):
	Global.mirror[2] = toggled_on


func _on_mirror_none_pressed():
	mirror_x.button_pressed = false
	mirror_y.button_pressed = false
	mirror_z.button_pressed = false


# Limit mouse wheel speed.
func _on_scroll_timer_timeout():
	can_scroll = true


func _on_color_picker_open():
	_log.debug("Color picker opened.")
	picking_color = true


func _on_color_changing(num, color):
	if picking_color and can_scroll: # I should make a new timer. But, I won't. Because I'm lazy :)
		can_scroll = false
		scroll_timer.start(scroll_time_min)
		Global.colors[num] = color
		colors_changing.emit()


# Color selected, or color picker updated.
func _on_color_set(num, color, changed):
	Global.active_color = num
	Global.colors[num] = color
	if changed:
		colors_changed.emit()
	await get_tree().process_frame


func _on_previous_background_pressed():
	_log.debug("Next Background Pressed")
	next_back.emit()


func _on_next_background_pressed():
	_log.debug("Previous Background Pressed")
	prev_back.emit()


func _on_dialog_canceled():
	Global.menu_open = false


func _on_back_culling_toggled(toggled_on):
	Global.back_culling = toggled_on
	culling_changed.emit()


func _on_color_picker_closed():
	picking_color = false
