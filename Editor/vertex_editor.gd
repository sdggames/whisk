class_name VertexEditor extends Node3D

const vertex := preload("res://Editor/vertex.tscn")
signal point_moved(Vertex)
signal point_clicked(Vertex)
signal point_modified
signal point_hidden
signal point_removed
signal action_cancelled

# Plane Objects
@onready var x_plane = $X_Plane
@onready var y_plane = $Y_Plane
@onready var z_plane = $Z_Plane

# Export Settings
@export var plane_movement_speed = 1.0  # Movement speed of the plane

# Active plane management
@export var selected_plane: AutoloadGlobal.Planes = Global.Planes.X:
	set(value):
		selected_plane = value
		_update_selected_plane()
var active_plane: Node3D # Currently selected plane
var plane_direction := Vector3.MODEL_LEFT
var other_planes = [] # Planes that are not currently selected

# Raycasting
var camera: Camera3D

# Vertex Management
var new_point: Vertex
var hovered_point: Vertex
var active_point: Vertex
var placed_points: Dictionary
var deleted_points: Dictionary


func _ready():
	camera = get_viewport().get_camera_3d()
	selected_plane = Global.Planes.X  # Initialize the selected plane
	_create_new_following_point()  # Create the first following point



## Update the position of the point as it collides with the plane.
func _process(_delta):
	if not new_point:
		return
	
	# We're moving the camera, don't ray cast.
	if Input.is_action_pressed("shift"):
		new_point.hide()
		return
	
	# Calculate the ray's origin and end points
	var from = camera.project_ray_origin(get_viewport().get_mouse_position())
	var to = from + camera.project_ray_normal(get_viewport().get_mouse_position()) * 1000

	# Set up ray query parameters
	var ray_query = PhysicsRayQueryParameters3D.new()
	ray_query.from = from
	ray_query.to = to
	if Global.mode == Global.Mode.PLACE:
		# Placing points, collide with points and planes.
		ray_query.collision_mask = 3
	elif active_point:
		# We are moving a point. Collide with plane only.
		ray_query.collision_mask = 1
	else:
		# We are looking for a point to modify. Collide with points only.
		ray_query.collision_mask = 2
	
	ray_query.exclude = other_planes + [new_point]
	if active_point:
		ray_query.exclude += [active_point]

	# Perform the raycast
	var space_state = get_world_3d().direct_space_state
	var intersection = space_state.intersect_ray(ray_query)
	
	if intersection:
		var p = intersection.collider
		if p is Vertex:
			if p != hovered_point and !p.is_queued_for_deletion():
				_try_exit_hover()
				new_point.hide()
				hovered_point = p
				emit_signal("point_moved", hovered_point)
				p.on_hover()
		else:
			_try_exit_hover()
			var point_position = intersection.position.round()  # Snap to nearest integer
			if Global.mode == Global.Mode.PLACE:
				new_point.show()
				new_point.position = point_position
				emit_signal("point_moved", new_point)
			if Global.mode == Global.Mode.MODIFY:
				active_point.position = point_position
				emit_signal("point_moved", active_point)
	else:
		_try_exit_hover()
		if new_point.visible:
			new_point.hide()
			emit_signal("point_hidden")


func click_left():
	if Global.mode == Global.Mode.PLACE:
		if hovered_point != null:
			hovered_point.activate()
			emit_signal("point_clicked", hovered_point)
		elif new_point.visible:
			emit_signal("point_clicked", _place_point_on_plane())
	elif Global.mode == Global.Mode.MODIFY:
		if hovered_point:
			active_point = hovered_point
			hovered_point = null
			active_point.start_drag()
			snap_all_planes(active_point.position)
			_update_selected_plane()
		elif active_point:
			active_point.place()
			active_point = null
			emit_signal("point_modified")
			hide_planes()


func click_right():
	if hovered_point:
		placed_points.erase(hovered_point.id)
		hovered_point.delete()
		deleted_points[hovered_point.id] = hovered_point
		hovered_point = null
		emit_signal("point_removed")
	elif Global.mode == Global.Mode.PLACE:
		deselect_all()
		emit_signal("action_cancelled")
	elif Global.mode == Global.Mode.MODIFY and active_point != null:
		active_point.cancel_drag()
		active_point = null
		emit_signal("action_cancelled")


func get_plane_pos() -> Vector3:
	return Vector3(x_plane.position.x, y_plane.position.y, z_plane.position.z)


func snap_all_planes(pos: Vector3):
	x_plane.position.x = pos.x
	y_plane.position.y = pos.y
	z_plane.position.z = pos.z


## Deactivate all currently hightlighted points.
func deselect_all():
	for p in placed_points.values():
		p.deactivate()


func _try_exit_hover():
	if hovered_point:
		emit_signal("point_hidden")
		hovered_point.exit_hover()
		hovered_point = null


func update_mode():
	if Global.mode == Global.Mode.MODIFY:
		hide_planes()
	else:
		_update_selected_plane()


## Move the plane forwards or backwards (-1 or +1).
func scroll_plane(i: int):
	if active_plane:
		active_plane.position += plane_direction * i
		if active_plane.position.length() > 50:
			active_plane.position = active_plane.position.normalized() * 50


## Hide all three planes without clearing selected_plane
func hide_planes():
	other_planes = [x_plane, y_plane, z_plane]
	for plane in other_planes:
		plane.visible = false


func _update_selected_plane():
	match selected_plane:
		Global.Planes.X:
			active_plane = x_plane
			plane_direction = Vector3.MODEL_LEFT
			other_planes = [y_plane, z_plane]
		Global.Planes.Y:
			active_plane = y_plane
			plane_direction = Vector3.MODEL_TOP
			other_planes = [x_plane, z_plane]
		Global.Planes.Z:
			active_plane = z_plane
			plane_direction = Vector3.MODEL_FRONT
			other_planes = [x_plane, y_plane]
		Global.Planes.NONE:
			active_plane = null
			other_planes = [x_plane, y_plane, z_plane]
	
	# Update plane visibility
	for plane in [x_plane, y_plane, z_plane]:
		plane.visible = plane == active_plane


func create_vert(id: int):
	# Create a new point.
	new_point = vertex.instantiate()
	new_point.id = id
	Global.all_points[id] = new_point
	add_child(new_point)


func _create_new_following_point():
	create_vert(Global.get_next_id())


func _place_point_on_plane() -> Vertex:
	var point = new_point
	new_point.place()
	new_point.activate()
	placed_points[new_point.id] = new_point
	_create_new_following_point()
	return point
