## Whisk is a 3D mesh editor. It's called Whisk because it isn't as powerful as Blender.
##
## This whole project could use a good refactoring, but I'm just glad that the proof-of-concept didn't
## take a second year. If this project is useful to you, feel free to change/fix things.
## Thank you, and I'm sorry :D
extends Node3D

@onready var menu = $Gui/HBoxContainer/Menu
@onready var vertex_editor = $VertexEditor
@onready var mesh_builder = $MeshBuilder
@onready var origin = $Static/Origin
@onready var data_hoarder = $Utilities/DataHoarder
@onready var mesh = $MeshBuilder/MeshScene/Mesh
var mode := Global.Mode.PLACE
var previous_pos: Vector3
var _log: Logger


func _ready():
	_log = Print.get_logger("Global")


func _on_menu_select_plane(plane):
	if plane == Global.Planes.NONE:
		if vertex_editor.selected_plane != Global.Planes.NONE:
			origin.visible = true
		else:
			origin.visible = !origin.visible
		vertex_editor.selected_plane = Global.Planes.NONE
	else:
		origin.visible = true
		if vertex_editor.selected_plane == plane:
			vertex_editor.selected_plane = Global.Planes.NONE
		else:
			vertex_editor.snap_all_planes(previous_pos)
			vertex_editor.selected_plane = plane
	mesh_builder.update_meshes()
	data_hoarder.create_undo_point()


func _on_menu_reset_plane():
	vertex_editor.snap_all_planes(Vector3.ZERO)


func _on_menu_revert_plane():
	vertex_editor.snap_all_planes(previous_pos)


func _on_vertex_editor_point_moved(v: Vertex):
	if v.position != previous_pos:
		previous_pos = v.position
		menu.update_active_point(v.position)
		mesh_builder.update_meshes(v)


func _on_vertex_editor_point_clicked(vertex):
	mesh_builder.add_point(vertex)
	data_hoarder.create_undo_point()


func _on_vertex_editor_point_removed():
	mesh_builder.remove_deleted()
	data_hoarder.create_undo_point()


func _on_vertex_editor_action_cancelled():
	if mesh_builder.clear_points():
		data_hoarder.create_undo_point()


func _on_vertex_editor_point_hidden():
	previous_pos = Vector3.ZERO
	mesh_builder.update_meshes()


func _on_data_hoarder_state_changed():
	get_tree().call_group("load_state", "load_state")
	origin.visible = true # This should be on by default, and nothing else sets it.
	mesh_builder.update_meshes()


func _on_menu_mode_changed():
	vertex_editor.update_mode()


func _on_vertex_editor_point_modified():
	data_hoarder.create_undo_point()
	mesh_builder.update_meshes()


func _on_menu_colors_changed():
	data_hoarder.create_undo_point()
	mesh_builder.update_meshes()


func _on_menu_colors_changing():
	mesh_builder.update_meshes()


func _on_menu_new():
	data_hoarder.reset()


func _on_menu_culling_changed():
	if Global.back_culling:
		mesh.material_override.cull_mode = BaseMaterial3D.CULL_BACK
	else:
		mesh.material_override.cull_mode = BaseMaterial3D.CULL_DISABLED
	data_hoarder.create_undo_point()
