class_name Triangle

## Stores the three points in a single triangle. Also indicates if this should be mirrored across the XY plane.
var point1: int
var point2: int
var point3: int
var color: int
var mirror: Array[bool] = [false, false, false]

var p1: Vector3:
	get:
		return Global.all_points[point1].grid_position
var p2: Vector3:
	get:
		return Global.all_points[point2].grid_position
var p3: Vector3:
	get:
		return Global.all_points[point3].grid_position
var is_invalid: bool:
	get:
		return Global.all_points[point1].deleted or Global.all_points[point2].deleted or Global.all_points[point3].deleted


func _init(pt1: int, pt2: int, pt3: int, vert_color: int, mirror_mode: Array[bool]):
	point1 = pt1
	point2 = pt2
	point3 = pt3
	color = vert_color
	mirror = mirror_mode.duplicate()


## Save this to an array that can be written to file.
func to_array() -> Array:
	return [point1, point2, point3, color, mirror]


## Load this from a previously saved data array.
static func from_array(arr: Array) -> Triangle:
	return Triangle.new(arr[0], arr[1], arr[2], arr[3], arr[4])
