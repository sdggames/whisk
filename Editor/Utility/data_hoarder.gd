extends Node

## Emitted when the state is loaded, or undo or redo is called.
signal state_changed

# public variables
var previous_states: Array[GameState] = []
var current_state := GameState.new()
var future_states: Array[GameState] = []
var initial_state: GameState

# private variables
var _log: Logger
var _applying_state := false

@onready var vertex_editor = $"../../VertexEditor"
@onready var mesh_builder = $"../../MeshBuilder"
@onready var mesh_scene = $"../../MeshBuilder/MeshScene"
@onready var mesh_instance: MeshInstance3D = $"../../MeshBuilder/MeshScene/Mesh"


func _ready():
	_log = Print.get_logger("Data")
	
	# Take our first state snapshot so we can reset to a blank canvas.
	await get_tree().process_frame
	snapshot()
	# Create a restore point if we want to start over.
	initial_state = current_state.copy()


## Clears all of the mesh and returns to the default.
func reset():
	current_state = initial_state.copy()
	previous_states.clear()
	future_states.clear()
	apply_state()


## Saves the previously snapshotted state to the stack, then takes a new snapshot.
func create_undo_point():
	if _applying_state:
		return
	checkpoint()
	snapshot()


## Capture the verts and triangles at this time.
func snapshot():
	# Update colors.
	current_state.colors = Global.colors.duplicate()
	
	# Update verts.
	current_state.next_vertex_id = Global.vertex_id
	
	if vertex_editor.active_point == null:
		current_state.active_point = []
	else:
		current_state.active_point = vertex_editor.active_point.parse_state()
	
	current_state.placed_points = []
	for p: Vertex in vertex_editor.placed_points.values():
		current_state.placed_points.append(p.parse_state())
	
	current_state.deleted_points = []
	for p: Vertex in vertex_editor.deleted_points.values():
		current_state.deleted_points.append(p.parse_state())
	
	current_state.back_culling = Global.back_culling
	
	current_state.selected_plane = int(vertex_editor.selected_plane)
	current_state.plane_pos = vertex_editor.get_plane_pos()
	current_state.anchor_point = -1 if mesh_builder.anchor_point == null else mesh_builder.anchor_point.id
	current_state.latest_point = -1 if mesh_builder.latest_point == null else mesh_builder.latest_point.id
	
	# Update tris.
	current_state.tris = []
	for t: Triangle in mesh_builder.tris:
		current_state.tris.append(t.to_array())


## Save the current state for undo/redo before making changes.
func checkpoint():
	previous_states.push_front(current_state.copy())
	future_states.clear()
	_log.verbose("Current state saved. There are %s previous states." % previous_states.size())


func apply_state():
	Global.colors = current_state.colors.duplicate()
	Global.back_culling = current_state.back_culling
	var verts_created := false
	
	# We don't want to ever reuse a vert. Since we always increment, we should just use the largest
	# id we've seen. This way, saving and loading won't clash with what is already instanced.
	Global.vertex_id = max(current_state.next_vertex_id, Global.vertex_id)
	
	# Clear out the tracked vertices, we will rebuld this.
	var all_points: Dictionary = Global.all_points.duplicate()
	vertex_editor.active_point = null
	vertex_editor.placed_points.clear()
	vertex_editor.deleted_points.clear()
	
	if current_state.active_point.size() == 3 and all_points.has(current_state.active_point[0]):
		var point: Vertex = vertex_editor.all_points[current_state.active_point[0]]
		all_points.erase(current_state.active_point[0])
		point.load_state(current_state.active_point)
		vertex_editor.active_point = point
	
	for point_state in current_state.placed_points:
		var id = point_state[0]
		if !all_points.has(id):
			verts_created = true
			vertex_editor.create_vert(id)
			vertex_editor.placed_points[id] = Global.all_points[id]
		else:
			vertex_editor.placed_points[id] = all_points[id]
			all_points.erase(id)
		vertex_editor.placed_points[id].load_state(point_state)
	
	for point_state in current_state.deleted_points:
		var id = point_state[0]
		if all_points.has(id):
			vertex_editor.deleted_points[id] = all_points[id]
			all_points.erase(id)
			vertex_editor.deleted_points[id].load_state(point_state)
	
	# Anything left over was added after this snapshot, and needs to be deleted.
	for point: Vertex in all_points.values():
		point.delete()
	
	# Create one more so we don't move an active vert around.
	if verts_created:
		vertex_editor.create_vert(Global.get_next_id())
	
	# Active plane.
	vertex_editor.selected_plane = Global.Planes.values()[current_state.selected_plane]
	vertex_editor.snap_all_planes(current_state.plane_pos)
	# Points referenced by mesh_builder. Can you tell that saving and loading was an afterthought?
	# Mesh builder points.
	mesh_builder.anchor_point = Global.all_points[current_state.anchor_point] if current_state.anchor_point >= 0 else null
	mesh_builder.latest_point = Global.all_points[current_state.latest_point] if current_state.latest_point >= 0 else null
	
	# Compared to verts, tris is easy!!!
		# Update tris.
	mesh_builder.tris = []
	for t in current_state.tris:
		mesh_builder.tris.append(Triangle.from_array(t))
	
	_log.verbose("Applying current state. There are %s previous states and %s future states." % [previous_states.size(), future_states.size()])
	_applying_state = true
	state_changed.emit()
	_applying_state = false


func undo():
	if _applying_state:
		return
	if previous_states.size() > 0:
		_log.info("Undo called.")
		var state = previous_states.pop_front()
		future_states.push_front(current_state)
		current_state = state
		apply_state()
	else:
		_log.info("Noting to undo.")


func redo():
	if _applying_state:
		return
	if future_states.size() > 0:
		_log.info("Redo called.")
		var state = future_states.pop_front()
		previous_states.push_front(current_state)
		current_state = state
		apply_state()
	else:
		_log.info("Noting to redo.")


func _on_save(file: String):
	_log.debug("Save command called. Attempting to save to " + file)
	var error = ResourceSaver.save(current_state, file)
	if error != OK:
		_log.error("Error saving game state: " + error_string(error))
	else:
		_log.info("Game state saved successfully.")


func _on_load(file: String):
	_log.debug("Load command called. Attempting to load from " + file)
	var loaded_state = ResourceLoader.load(file)
	if loaded_state == null:
		_log.error("Error loading game state.")
	elif loaded_state is GameState:
		# We don't save undo-redo info, so clear it.
		previous_states.clear()
		future_states.clear()
		current_state = loaded_state
		apply_state()
		_log.info("Game state loaded successfully.")
	else:
		_log.error("Loaded resource is not a GameState.")


func _on_export(file: String):
	_log.debug("Export command called. Attempting to export to " + file)
	
	# Ensure the mesh_instance is valid
	if mesh_scene == null:
		print("MeshInstance is null. Please assign a MeshInstance to the variable.")
		return
	
	var file_extension = file.get_extension().to_lower()

	match file_extension:
		"tscn":
			_export_as_tscn(file)
		"dat":
			_export_as_dat(file)
		"glb", "gltf":
			_export_as_gltf(file)
		"obj":
			_export_as_obj(file)
		_:
			print("Unsupported file extension: " + file_extension)


func _export_as_tscn(file: String):
	var packed_scene = PackedScene.new()
	packed_scene.pack(mesh_instance)
	ResourceSaver.save(packed_scene, file)
	print("MeshInstance exported successfully to ", file)


func _export_as_dat(file: String):
	var file_dat = FileAccess.open(file, FileAccess.WRITE)
	if file_dat == null:
		print("Failed to open file: ", file)
		return

	file_dat.store_string("Custom data format export for mesh_instance")
	file_dat.close()
	print("MeshInstance exported successfully to ", file)


func _export_as_gltf(file: String):
	var gltf_document = GLTFDocument.new()
	var gltf_state = GLTFState.new()
	gltf_document.append_from_scene(mesh_scene, gltf_state)
	var error = gltf_document.write_to_filesystem(gltf_state, file)
	if error == OK:
		_log.info("MeshInstance exported successfully to " + file)
	else:
		_log.error("Failed to export MeshInstance. Error code: " + error_string(error))


func _export_as_obj(file: String):
	var file_obj = FileAccess.open(file, FileAccess.WRITE)
	if file_obj == null:
		return

	file_obj.store_line("# Exported from Godot")
	
	var vertices = mesh_builder.vertices
	var normals = mesh_builder.normals
	var indices = mesh_builder.indices

	# Write vertices
	for v in vertices:
		file_obj.store_line("v %f %f %f" % [v.x, v.y, v.z])

	# Write normals, if they exist
	if normals:
		for n in normals:
			file_obj.store_line("vn %f %f %f" % [n.x, n.y, n.z])

	# Write faces
	if indices and indices.size() > 0:
		for j in range(0, indices.size(), 3):
			var v1 = indices[j] + 1
			var v2 = indices[j + 1] + 1
			var v3 = indices[j + 2] + 1
			if normals:
				file_obj.store_line("f %d//%d %d//%d %d//%d" % [v1, v1, v2, v2, v3, v3])
			else:
				file_obj.store_line("f %d %d %d" % [v1, v2, v3])

	file_obj.close()
	_log.info("MeshInstance exported successfully to " + file)
