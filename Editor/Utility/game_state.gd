class_name GameState extends Resource

# !! IMPORTANT: Make sure to set new types in snapshot(), clone in copy(), and restore in apply_state(). !!
# Colors
@export var colors: Array[Color]

# UUID for verts.
@export var next_vertex_id := 0

# Verts
@export var active_point: Array = []
@export var placed_points: Array = []
@export var deleted_points: Array = []

# Misc.
@export var selected_plane: int
@export var plane_pos: Vector3
@export var anchor_point: int
@export var latest_point: int
@export var back_culling: bool

# Tris
@export var tris: Array

func copy() -> GameState:
	var state = GameState.new()
	state.colors = colors.duplicate()
	state.next_vertex_id = next_vertex_id
	
	state.active_point = active_point
	state.placed_points = placed_points.duplicate()
	state.deleted_points = deleted_points.duplicate()
	state.back_culling = back_culling
	
	state.selected_plane = selected_plane
	state.plane_pos = plane_pos
	state.anchor_point = anchor_point
	state.latest_point = latest_point
	
	state.tris = tris.duplicate()
	return state
