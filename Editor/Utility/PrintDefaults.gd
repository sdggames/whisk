## Stores the print defaults in an easily accessible node in the main game scene. Sends the names
## to the Global singleton to preload the loggers, but overwrites their values later when this
## class is added to the scene tree. After doing its job, the class is removed so I don't try to
## change the values here while the game is running (which does nothing, [Print] is maintaining the
## loggers at that point.)
class_name PrintDefaults extends Node

## Defines all of the loggers in the project. All loggers should be defined in this const so there
## are no initialization errors. We can safely print from [method _ready] functions this way.
## All loggers will be set to [constant default_console_level] [constant default_archive_level]
## by the Global singleton, but will be reset to their proper values when this node is added
## to the scene.
const print_loggers = [
	# General
	"Global", # Any Print.level() calls
	"Menu", # Input or button handling
	"Data", # Undo, Redo, Save/Load
]

## The default logging level for all loggers in the [constant print_loggers] array. Is overridden when
## the [PrintDefaults] object loads in.
const default_console_level := Logger.LogLevel.INFO

## The default archive level for all loggers in the [constant print_loggers] array. Is overridden when
## the [PrintDefaults] object loads in.
const default_archive_level := Logger.LogLevel.VERBOSE

@export_group("Global Logger", "global_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var global_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var global_archive_level := 5

@export_group("Print Logger", "print_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var print_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var print_archive_level := 5

@export_group("Menu Logger", "menu_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var menu_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var menu_archive_level := 5

@export_group("Data Logger", "data_")
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var data_console_level := 2
@export_enum("Silent", "Error", "Warning", "Info", "Debug", "Verbose") var data_archive_level := 5

func _ready():
	Print.create_logger("Global", global_console_level, global_archive_level)
	Print.create_logger("Menu", menu_console_level, menu_archive_level)
	Print.create_logger("Data", data_console_level, data_archive_level)
	# Last so we don't clutter the output with all the loggers we just registered.
	# This one only outputs registered loggers, 
	Print.create_logger("Print", print_console_level, print_archive_level)
	# Our work here is done.
	queue_free()
