class_name MeshBuilder extends Node3D

@onready var mesh = $MeshScene/Mesh

var vertices = []
var indices = []
var normals = []
var surface: SurfaceTool
var array_mesh: ArrayMesh
var tris: Array

var anchor_point: Vertex
var latest_point: Vertex


## Set up the surface tools.
func _ready():
	surface = SurfaceTool.new()
	tris = []


## Add a point to the triangle that we are actively building.
func add_point(point: Vertex):
	match Global.place_mode:
		Global.PlaceMode.SINGLE:
			# We are placing points, but we don't interact with them.
			clear_points(point)
		Global.PlaceMode.TRIANGLE:
			if anchor_point != null and latest_point != null:
				_build_triangle(anchor_point, latest_point, point)
				clear_points(point)
			elif anchor_point != null:
				latest_point = point
			else:
				anchor_point = point
		Global.PlaceMode.RADIAL:
			if anchor_point == null:
				anchor_point = point
			elif latest_point == null:
				latest_point = point
			else:
				_build_triangle(anchor_point, latest_point, point)
				latest_point.deactivate()
				latest_point = point
			update_meshes()
		Global.PlaceMode.ZIPPER:
			if anchor_point == null:
				anchor_point = point
			elif latest_point == null:
				latest_point = point
			else:
				_build_triangle(anchor_point, latest_point, point)
				anchor_point.deactivate()
				anchor_point = latest_point
				latest_point = point
			update_meshes()


## Remove all tracked points from the current triangle.
## Returns true if the points existed before the call.
func clear_points(point: Vertex = null) -> bool:
	var state_changed = false
	if point != null:
		point.deactivate()
	if anchor_point != null:
		anchor_point.deactivate()
		anchor_point = null
		state_changed = true
	if latest_point != null:
		latest_point.deactivate()
		latest_point = null
		state_changed = true
	update_meshes()
	return state_changed


## Remove all triangles that have vertices pending deletion.
func remove_deleted():
	var valid_tris = []
	for triangle in tris:
		if not triangle.is_invalid:
			valid_tris.append(triangle)
	tris = valid_tris

	# Remove invalid vertices from builder_points.
	if anchor_point != null and anchor_point.is_queued_for_deletion():
		anchor_point = null
	if latest_point != null and latest_point.is_queued_for_deletion():
		latest_point = null
	if latest_point != null and anchor_point == null:
		anchor_point = latest_point
		latest_point = null
	update_meshes()


## Save this triangle to the list of built tris.
func _build_triangle(anchor, latest, newest):
	# Calculate the normal of the triangle
	var normal = (latest.position - anchor.position).cross(newest.position - anchor.position).normalized()
	
	# Get the camera's forward direction
	var camera = get_viewport().get_camera_3d()
	var view_direction = (anchor.position - camera.global_transform.origin).normalized()
	
	# If the normal is not facing the camera, flip the vertices
	if normal.dot(view_direction) > 0:
		tris.append(Triangle.new(newest.id, anchor.id, latest.id, Global.active_color, Global.mirror))
	else:
		tris.append(Triangle.new(latest.id, anchor.id, newest.id, Global.active_color, Global.mirror))


## Rebuild the meshes to keep up with the points moving on screen.
func update_meshes(v: Vertex = null):
	# Clear previous data
	vertices.clear()
	indices.clear()
	normals.clear()
	
	# Update the mesh
	surface.clear()
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface.set_smooth_group(-1)
	
	# Draw only if we have all three points. Draw both sides so we don't have to test winding order.
	surface.set_color(Global.colors[Global.active_color])
	if v != null and anchor_point != null and latest_point != null:
		_add_all_triangle_mirrors(v.grid_position, anchor_point.grid_position, latest_point.grid_position, Global.mirror)
		_add_all_triangle_mirrors(latest_point.grid_position, anchor_point.grid_position, v.grid_position, Global.mirror)
	
	for t: Triangle in tris:
		surface.set_color(Global.colors[t.color])
		_add_all_triangle_mirrors(t.p1, t.p2, t.p3, t.mirror)
	
	surface.generate_normals()
	
	surface.commit(mesh.get_mesh())
	mesh.set_mesh(surface.commit())


func _add_all_triangle_mirrors(p1: Vector3, p2: Vector3, p3: Vector3, mirrors: Array[bool]):
	_add_triangle(p1, p2, p3)
	if mirrors[0]: # Mirror across the X axis.
		_add_all_triangle_mirrors(p3 * Vector3(-1, 1, 1), p2 * Vector3(-1, 1, 1), p1 * Vector3(-1, 1, 1), [false, mirrors[1], mirrors[2]])
	if mirrors[1]: # Mirror across the Y axis.
		_add_all_triangle_mirrors(p3 * Vector3(1, -1, 1), p2 * Vector3(1, -1, 1), p1 * Vector3(1, -1, 1), [false, false, mirrors[2]])
	if mirrors[2]:
		_add_triangle(p3 * Vector3(1, 1, -1), p2 * Vector3(1, 1, -1), p1 * Vector3(1, 1, -1))


# Add a single triangle to the mesh. Draws both sides of the triangle.
func _add_triangle(p1: Vector3, p2: Vector3, p3: Vector3):
	var idx_start = vertices.size()
	
	# Add all three points.
	surface.add_vertex(p1)
	surface.add_vertex(p2)
	surface.add_vertex(p3)
	
	# Used for exporting to .obj. This is more complex, but allows us to share verts.
	if !vertices.has(p1):
		vertices.append(p1)
	if !vertices.has(p2):
		vertices.append(p2)
	if !vertices.has(p3):
		vertices.append(p3)
	
	# Flip normals. Apparently, the winding order is reversed here.
	indices.append(vertices.find(p3))
	indices.append(vertices.find(p2))
	indices.append(vertices.find(p1))
