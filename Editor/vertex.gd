class_name Vertex extends StaticBody3D

enum VertexState {
	NOT_PLACED,
	PLACED,
	ACTIVE,
	HIGHLIGHT,
	EDIT,
	DELETED,
}

var id: int
var used := false
var last_pos: Vector3
var state := VertexState.NOT_PLACED:
	set(value):
		state = value
		make_invisible()
		match value:
			VertexState.NOT_PLACED:
				collision_layer = 0
				not_placed.visible = true
			VertexState.PLACED:
				collision_layer = 2
				placed.visible = true
			VertexState.ACTIVE:
				collision_layer = 2
				active.visible = true
			VertexState.EDIT:
				collision_layer = 2
				edit.visible = true
			VertexState.DELETED:
				collision_layer = 0

var selected: bool:
	get:
		return state == VertexState.ACTIVE or state == VertexState.EDIT

var grid_position: Vector3:
	get:
		return global_position

var deleted: bool :
	get:
		return state == VertexState.DELETED

@onready var not_placed = $"Not Placed"
@onready var placed = $Placed
@onready var active = $Active
@onready var highlight = $Highlight
@onready var edit = $Edit


## Set this to a floating vertex.
func _ready():
	state = VertexState.NOT_PLACED


## Return the raw data from this vert. Can be loaded back into a vertex with load_state().
func parse_state():
	return [id, position, state]


func load_state(arr: Array):
	id = arr[0]
	position = arr[1]
	state = arr[2]


## Place this vertex in the world, turning it on.
func place():
	last_pos = position
	state = VertexState.PLACED


## Color this vertex while it is being used.
func activate():
	state = VertexState.ACTIVE


## Start moving this point in 3D space. End the drag by calling place()
func start_drag():
	state = VertexState.EDIT


## Cancel moving this point. Reset the position to the start of the drag.
func cancel_drag():
	position = last_pos
	state = VertexState.PLACED


## Stop coloring this vertex.
func deactivate():
	state = VertexState.PLACED


## Mouse over this node. Disabled if this node is already active.
func on_hover():
	if !active.visible:
		highlight.visible = true


## Stop mousing over this node.
func exit_hover():
	highlight.visible = false


## Remove this vertex. Keep im memory in case it needs to be reactivated.
func delete():
	state = VertexState.DELETED


## Turn everything off.
func make_invisible():
	not_placed.visible = false
	placed.visible = false
	active.visible = false
	highlight.visible = false
	edit.visible = false
