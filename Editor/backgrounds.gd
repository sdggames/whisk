extends WorldEnvironment

@export var hdris: Array[Resource] = [null]
var current_back: int = 0


func _ready():
	environment.sky = Sky.new()
	environment.sky.sky_material = PanoramaSkyMaterial.new()


func _on_menu_next_back():
	current_back = wrapi(current_back - 1, 0, hdris.size())
	_set_back()


func _on_menu_prev_back():
	current_back = wrapi(current_back + 1, 0, hdris.size())
	_set_back()


func _set_back():
	if hdris[current_back] == null:
		environment.background_mode = Environment.BG_CLEAR_COLOR
	else:
		environment.background_mode = Environment.BG_SKY
		environment.sky.sky_material.panorama = hdris[current_back]
